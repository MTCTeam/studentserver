package com.studentgrade.studentserver.controllers;

import com.studentgrade.studentserver.models.ClassModel;
import com.studentgrade.studentserver.models.GradesModel;
import com.studentgrade.studentserver.models.StudentModel;
import com.studentgrade.studentserver.repositories.ClassRepository;
import com.studentgrade.studentserver.repositories.GradesRepository;
import com.studentgrade.studentserver.repositories.StudentRepository;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin
public class StudentController {

    // ***** Member Variables *****
    private final StudentRepository studentRepo;
    private final ClassRepository classRepo;
    private final GradesRepository gradesRepo;


    // ***** Constructors *****
    public StudentController(StudentRepository studentRepo, ClassRepository classRepo, GradesRepository gradesRepo) {
        this.classRepo = classRepo;
        this.studentRepo = studentRepo;
        this.gradesRepo = gradesRepo;

    }

    // ***** Functions *****
    @GetMapping("/students")
    public List<StudentModel> getAllStudents() {return (List<StudentModel>) this.studentRepo.findAll(); }


    @GetMapping("/classes")
    public List<ClassModel> getAllClasses() { return (List<ClassModel>) this.classRepo.findAll(); }


    @GetMapping("/student/average")
    public HashMap<Long, Double> getStudentGrades() {
        HashMap<Long, Double> studentGPAs = new HashMap<>();
        Iterable<StudentModel> allStudents = studentRepo.findAll();

        for (StudentModel student : allStudents) {
            List<GradesModel> grades = student.getGrades();
            double sum = 0.0;

            for(GradesModel grade : grades) {
                    sum += grade.getGrade();
            }
            double gpa = sum / student.getGrades().size();
            studentGPAs.put(student.getId(), gpa);
        }

        return studentGPAs; }


    @PostMapping("/create")
     public int createItem(@RequestBody StudentModel item){
        this.studentRepo.save(item);
        return 0;
     }


    @PostMapping("/addGPA")
    public int addGPA(@RequestBody GradesModel item){
        this.gradesRepo.save(item);
        return 0;
    }

}
