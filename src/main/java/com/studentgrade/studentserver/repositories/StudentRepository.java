package com.studentgrade.studentserver.repositories;

import com.studentgrade.studentserver.models.GradesModel;
import com.studentgrade.studentserver.models.StudentModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StudentRepository extends CrudRepository<StudentModel, Long> {

}
