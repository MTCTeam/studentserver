package com.studentgrade.studentserver.repositories;

import com.studentgrade.studentserver.models.ClassModel;
import org.springframework.data.repository.CrudRepository;

public interface ClassRepository extends CrudRepository<ClassModel, Long> {
}
