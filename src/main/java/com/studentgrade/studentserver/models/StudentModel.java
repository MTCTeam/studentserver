package com.studentgrade.studentserver.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class StudentModel {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @ManyToOne
    @JoinColumn(name ="classModel", referencedColumnName = "Id")
    private ClassModel classModel;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "student")
    private List<GradesModel> grades;


    public List<GradesModel> getGrades()
    {
        return grades;
    }

    public void setGrades(List<GradesModel> grades)
    {
        this.grades = grades;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

}
